package resources;

public class Player {
	private int ID = 0;
	private String name;
	private String attack;
	private String defense;
	private String target;
	private int attackSpd;
	private int defenseSpd;
	private int roll;
	private int wound;
	
	
	public Player(String name) {
		this.name = name;
	}
	
	public int getId () {
		return this.ID;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getAttack() {
		return this.attack;
	}
	
	public String getDefense() {
		return this.defense;
	}
	
	public String getTarget() {
		return this.target;
	}
	
	public int getAttackSpd() {
		return this.attackSpd;
	}
	
	public int getDefenseSpd() {
		return this.defenseSpd;
	}
	
	public int getRoll() {
		return this.roll;
	}
	
	public int getWound() {
		return this.wound;
	}
	
	public void incrementWound() {
		wound ++;
	}
	
	public void setId (int id) {
		this.ID = id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setAttack(String attack) {
		this.attack = attack;
	}
	
	public void setDefense(String defense) {
		this.defense = defense;
	}
	
	public void setTarget(String target) {
		this.target = target;
	}
	
	public void setAttackSpd(int attackSpd) {
		this.attackSpd = attackSpd;
	}
	
	public void setDefenseSpd(int defenseSpd) {
		this.defenseSpd = defenseSpd;
	}
	
	public void setRoll(int roll) {
		this.roll = roll;
	}
	
	public void setWound(int wound) {
		this.wound = wound;
	}
}
