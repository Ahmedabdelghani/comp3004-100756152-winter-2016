package rulesengine;
import utils.Config;

public class GameRulesEngine {
	
    private int state = Config.WAITING;
    
    public String[] attacks = {"Thrust", "Swing", "Smash"};
    public String[] defenses = {"Charge", "Dodge", "Duck"};
    
    
    public GameRulesEngine() {
    	System.out.println("Creating Game Rules engine. \n");
    }

    
    public int processGameState(int numPlayers) {
    	String output = "waiting for players.\n";
    	
    	if (state == Config.WAITING) {
    		if (numPlayers < Config.MINIMUM_PLAYERS) {
    			state = Config.WAITING;
    		}
    		else if (numPlayers > Config.MAXIMUM_PLAYERS) {
    			state = Config.OVER_CAPACITY;
    			output = "There are too many players\n";
    		}
    		else {
    			state = Config.READY;
    			output = "Ready to start the game\n";
    		}
    	}
    	
    	else if (state == Config.OVER_CAPACITY) {
    		if (numPlayers <= Config.MAXIMUM_PLAYERS && numPlayers >= Config.MINIMUM_PLAYERS) {
    			state = Config.READY;
    			output = "Ready to start the game\n";
    		}
    		else {
    			state = Config.WAITING;
    			output = "waiting for players.\n";
    		}
    	}
    	
    	else if (state == Config.READY) {
    		if (numPlayers >= Config.MINIMUM_PLAYERS && numPlayers <= Config.MAXIMUM_PLAYERS) {
    			state = Config.READY;
    			output = "Ready to start the game\n";
    		}
    		else {
    			state = Config.WAITING;
    			output = "waiting for players.\n";
    		}
    	}
    	
    	else if (state == Config.ANOTHER) {
    		
    	}
    	
    	else {
    		output = "Good Bye! \n";
    		state = Config.WAITING;
    	}
    	return state;
    }
    
    
    public String processRoll (String attack, int roll) {
    	if ((roll == 3) || (roll == 4)) {
    		if (attack.equalsIgnoreCase(attacks[0])) {
    			attack = attacks[2];
    		}
    		else if (attack.equalsIgnoreCase(attacks[1])) {
    			attack = attacks[0];
    		}
    		else {
    			attack = attacks[1];
    		}
    	}
    	
    	else if ((roll == 5) || (roll == 6)) {
    		if (attack.equalsIgnoreCase(attacks[0])) {
    			attack = attacks[1];
    		}
    		else if (attack.equalsIgnoreCase(attacks[1])) {
    			attack = attacks[2];
    		}
    		else {
    			attack = attacks[0];
    		}
    	}
    	else if ((roll == 1) || (roll == 2)) {
    		return attack;
    	}
    	return attack;

    }
    
    
    public Boolean processGameLogic(String attack, int attackSpd, String defense, int defenseSpd) {
    	Boolean hit;
    	if((attack.equalsIgnoreCase(attacks[0]) && defense.equalsIgnoreCase(defenses[0])) || attackSpd < defenseSpd) {
    		hit = true;
    	}
    	
    	else if((attack.equalsIgnoreCase(attacks[1]) && defense.equalsIgnoreCase(defenses[1])) || attackSpd < defenseSpd) {
    		hit = true;
    	}
    	
    	else if((attack.equalsIgnoreCase(attacks[2]) && defense.equalsIgnoreCase(defenses[2])) || attackSpd < defenseSpd) {
    		hit = true;
    	}
    	else hit = false;
    	
    	return hit;
    }
    
    public Boolean processGameLogicSpeed(int attackSpd, int defenseSpd) {
    	Boolean hit;
    	if (attackSpd < defenseSpd) {
    		hit = true;
    	}
    	else hit = false;
    	return hit;
    }
    
    public void setState (int state) {
    	this.state = state;
    }
    
    public void arrayCoounter(int i) {
    	
    }
}
