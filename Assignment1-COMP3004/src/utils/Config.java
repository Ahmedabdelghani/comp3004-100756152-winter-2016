package utils;

import java.util.ArrayList;


public class Config {
	public static final int DEFAULT_PORT = 3000;
	public static final String DEFAULT_HOST = "127.0.0.1";
	public static final String DEFAULT_USER = "Ducky";
	public static final int MINIMUM_PLAYERS = 2;
    public static final int MAXIMUM_PLAYERS = 4;
    public static final int WAITING = 0;
    public static final int READY = 1;
    public static final int OVER_CAPACITY = 2;
    public static final int SENTPING = 3;
    public static final int SENTCLUE = 4;
    public static final int ANOTHER = 5;
	public static final boolean PRINT_STACK_TRACE = false;
	
	public static final String SWING = "swing";
	public static final String THRUST = "thrust";
	public static final String SMASH = "smash";
	public static final String DUCK = "duck";
	public static final String DODGE = "dodge";
	public static final String CHARGE = "charge";
	
}
