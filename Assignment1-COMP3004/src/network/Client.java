package network;

import java.net.*;
import java.io.*;
import org.apache.log4j.Logger;

import utils.Config;
import utils.Trace;

public class Client implements Runnable {
	private int ID = 0;
	private static String name;
	private Socket socket            = null;
	private Thread thread            = null;
	private ClientThread   client    = null;
	private BufferedReader console   = null;
	private BufferedReader streamIn  = null;
	private BufferedWriter streamOut = null;
	private Logger log = Logger.getLogger("Client");
	
	public Client (String serverName, int serverPort, String name) {  
		System.out.println(ID + ": Establishing connection. Please wait ...");
		log.info(ID + ": Establishing connection. Please wait ...");
		this.name = name;

		try {  
			this.socket = new Socket(serverName, serverPort);
			this.ID = socket.getLocalPort();
	    	System.out.println(ID + ": Connected to server: " + socket.getInetAddress());
	    	log.info(ID + ": Connected to server: " + socket.getInetAddress());
	    	System.out.println(ID + ": Connected to portid: " + socket.getLocalPort());
	    	log.info(ID + ": Connected to portid: " + socket.getLocalPort());
	      this.start();
		} catch(UnknownHostException uhe) {  
			System.err.println(ID + ": Unknown Host");
			log.info(ID + ": Unknown Host");
			Trace.getInstance().exception(this,uhe);
		} catch(IOException ioe) {  
			System.out.println(ID + ": Unexpected exception");
			log.info(ID + ": Unexpected exception");
			Trace.getInstance().exception(this,ioe);
	   }
	}

	public int getID () {
		return this.ID;
	}
	
   public void start() throws IOException {  
	   try {
	   	console	= new BufferedReader(new InputStreamReader(System.in));
		   streamIn	= new BufferedReader(new InputStreamReader(socket.getInputStream()));
		   streamOut = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

		   if (thread == null && name != null) {
		   	  client = new ClientThread(this, socket);
		      thread = new Thread(this);                   
		      thread.start();
		   }
	   } catch (IOException ioe) {
      	Trace.getInstance().exception(this,ioe);
         throw ioe;
	   }
   }

	public void run() { 
		System.out.println(ID + ": Client Started...");
		log.info(ID + ": Client Started...");
		send("join " + name);
		log.info("join " + name);
		while (thread != null) {
			try {  
				if (streamOut != null) {
					streamOut.flush();
					streamOut.write(console.readLine() + "\n");
				} else {
					System.out.println(ID + ": Stream Closed");
					log.info(ID + ": Stream Closed");
				}
			}
         catch(IOException e) {
         	Trace.getInstance().exception(this,e);
         	stop();
         }}
		System.out.println(ID + ": Client Stopped...");
		log.info(ID + ": Client Stopped...");
   }
	
	public void send(String msg) {
		try {
			streamOut.flush();
			streamOut.write(msg + "\n");
		} catch (IOException ioe) {
			Trace.getInstance().exception(this,ioe,ID);
		}
	}

   public void handle (String msg) {
   	if (msg.equalsIgnoreCase("quit!")) {  
			System.out.println(ID + "Good bye. Press RETURN to exit ...");
			log.info(ID + "Good bye. Press RETURN to exit ...");
			stop();
	}
   	else if(msg.equals("READY")) {
   		System.out.println("\nPlease enter your attack plan starting with select");
   		log.info("\nPlease enter your attack plan starting with select");
   		//to send something to the server just call send from here
   		
   	}
   	else if(msg.equalsIgnoreCase("maximumReached")) {
   		System.out.println("Sorry, Maximum number of players reached");
   		log.info("Sorry, Maximum number of players reached");
   		//to send something to the server just call send from here
   		
   	}
   	else if (msg.equalsIgnoreCase("getName")) {
   		send("rolling " + name);
   		log.info("rolling " + name);
   	}
   	else {
			
			System.out.println(msg);
			log.info(msg);
		}
   }

   public void stop() {  
      try { 
      	if (thread != null) thread = null;
    	  	if (console != null) console.close();
    	  	if (streamIn != null) streamIn.close();
    	  	if (streamOut != null) streamOut.close();

    	  	if (socket != null) socket.close();

    	  	this.socket = null;
    	  	this.console = null;
    	  	this.streamIn = null;
    	  	this.streamOut = null;    	  
      } catch(IOException ioe) {  
    	  Trace.getInstance().exception(this,ioe);
      }
      client.close();  
   }

   @SuppressWarnings("unused")
	public static void main(String args[]) {  
   	Client client = new Client(Config.DEFAULT_HOST, Config.DEFAULT_PORT, name); 
   }
}
