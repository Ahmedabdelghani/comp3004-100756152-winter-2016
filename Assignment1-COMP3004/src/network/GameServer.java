package network;

import java.net.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import resources.Player;
import rulesengine.GameRulesEngine;

import org.apache.log4j.Logger;

import java.io.*;

import utils.Config;
import utils.Trace;

public class GameServer implements Runnable {
	int clientCount = 0;
	GameRulesEngine engine                   = new GameRulesEngine();
	private static Scanner sc                = new Scanner(System.in);
	int numPlayers                           = 0;
	int rounds                               = 0;
	int selectDone                           = 0;
	int rollDone                             = 0;
	int roundsDone                           = 0;
	private Thread thread                    = null;
	private ServerSocket server              = null;
	private BufferedReader console           = null;
	private BufferedReader streamIn          = null;
	private BufferedWriter streamOut         = null;
	private ArrayList<Player> players = new ArrayList<Player>();
	private HashMap<Integer, ServerThread> clients;
	private Logger log = Logger.getLogger("Server");
	
	public GameServer(int port, int numPlayers, int rounds) {
		try {
			this.numPlayers = numPlayers;
			this.rounds = rounds;
			System.out.println("Binding to port " + port + ", please wait  ...");
			log.info("Binding to port " + port + ", please wait  ...");
			Trace.getInstance().write(this, "Binding to port " + port);
			log.info("Binding to port " + port);
			clients = new HashMap<Integer, ServerThread>();

			server = new ServerSocket(port);
			server.setReuseAddress(true);
			start();
		} catch (IOException ioe) {
			Trace.getInstance().exception(ioe);
			log.fatal(ioe);
		}
	}

	/** Now we start the servers main thread */
	public void start() {
		if (thread == null) {
			thread = new Thread(this);
			thread.start();
		}
	}

	/** The main server thread starts and is listening for clients to connect */
	public void run() {
		
		while (thread != null && clients.size() < numPlayers) {
			try {
				Trace.getInstance().write(this, "Waiting for a client ...");
				log.info("Waiting for a client ...");
				addThread(server.accept());
			} catch (IOException e) {				
				Trace.getInstance().exception(this,e);
			}}
	}

	/** 
	 * Client connection is accepted and now we need to handle it and register it 
	 * and with the server | HashTable 
	 **/
	private void addThread(Socket socket) {
		Trace.getInstance().write(this, "Client accepted", socket);
		if (clientCount < numPlayers || clientCount == 0) {
			try {
				/** Create a separate server thread for each client */
				ServerThread serverThread = new ServerThread(this, socket);
				/** Open and start the thread */
				serverThread.open();
				serverThread.start();
				
				clients.put(serverThread.getID(), serverThread);
				this.clientCount++;
			} catch (IOException e) {
				Trace.getInstance().exception(this,e);
			}
		} else {
			log.info(String.format("Client Tried to connect: %s", socket));
			log.info(String.format("Client refused, specified number of players is " + numPlayers));

			Trace.getInstance().write(this, "Client Tried to connect", socket );
			Trace.getInstance().write(this, "Client refused: specified number of players reached", numPlayers );
		}
	}

	public synchronized void handle(int ID, String input) {
		if (input.equals("quit!")) 
		{
			broadcast(ID + input);
			log.info(ID+input);
			log.info("quitting");
			log.info(String.format("Removing Client: %d", ID));
			Trace.getInstance().write(this,"Removing Client:", ID);
			if (clients.containsKey(ID)) {
				clients.get(ID).send("quit!" + "\n");
				remove(ID);
			}}
		else if (input.equals("shutdown!")) {
			broadcast(ID + input);
			log.info(ID+input);
			shutdown(); 
			log.info("shutting down");
		}
		
		else if(input.contains("join")) {
			broadcast(ID + input);
			log.info(ID+input);
			
			if(players.size() < numPlayers) {
				String clientName[] = input.split(" ");
				Player player = new Player(clientName[1]);
				players.add(player);
				if(players.size() == numPlayers) {
					broadcast("\nThe available players are:");
					log.info("\nThe available players are:");
					for(int i = 0; i < players.size(); i++) {
						broadcast(String.valueOf(players.get(i).getName()));
						log.info(String.valueOf(players.get(i).getName()));
					}
					broadcast("READY");
				}
			}
			else {
				broadcast(ID + input);
				log.info(ID+input);
				String clientName[] = input.split(" ");
				Player kickedOut = new Player(clientName[1]);
				players.add(kickedOut);
				sendToOne(ID, "SORRY YOU ARE OVER CAPACITY");
				log.info(ID + ", SORRY YOU ARE OVER CAPACITY");
				remove(ID);
				players.remove(players.size()-1);
				System.out.println("REMAINING PLAYERS ARE: ");
				for(int i = 0; i < players.size(); i++) {
					System.out.println(String.valueOf(players.get(i).getName()));
				}
			}
			
		}

		else if(input.startsWith("select"))
		{
			broadcast(ID + input);
			log.info(ID+input);
			ServerThread from = clients.get(ID);
				String playerInfoArray[] = input.split(" ");
				for(int i = 0; i < players.size(); i++) {
					if(players.get(i).getName().contains(String.valueOf(playerInfoArray[1]))) {
						int attackSpeed = Integer.valueOf((playerInfoArray[4]));
						int defenseSpeed = Integer.valueOf((playerInfoArray[6]));
						if((attackSpeed + defenseSpeed) < 4) {
							sendToOne(ID, "\nYour attack speed and defense speed must add up to at least 4, please try again");
							log.info(ID + "\nYour attack speed and defense speed must add up to at least 4, please try again");
						}
						else {
							players.get(i).setTarget(playerInfoArray[2]);
							players.get(i).setAttack(playerInfoArray[3]);
							players.get(i).setAttackSpd(Integer.valueOf(playerInfoArray[4]));
							players.get(i).setDefense(playerInfoArray[5]);
							players.get(i).setDefenseSpd(Integer.valueOf(playerInfoArray[6]));
							selectDone++;
							if(selectDone < players.size()) {
								sendToOne(ID, "Please wait until everyone is finished");
								log.info(ID + ", Please wait until everyone is finished");
							}
						}
					}
				}
			if(selectDone == players.size()) {
				broadcast(ID + input);
				log.info(ID+input);
				broadcast("\nPlease Roll");
				log.info("\nPlease Roll");
			}
		}//}
		
		else if(input.contains("roll")) {
			broadcast(ID + input);
			log.info(ID+input);
			String roll[] = input.split(" ");
			int rolled = Integer.valueOf(roll[2]);
			if (Integer.valueOf(roll[2]) < 7 && rolled > 0) {
				rollDone++;
				for(int i = 0; i < players.size(); i++) {
					if(players.get(i).getName().equals(String.valueOf(roll[1]))) {
						players.get(i).setRoll(rolled);
						players.get(i).setAttack(engine.processRoll(players.get(i).getAttack(), rolled));
					}
				}
				if(rollDone < players.size()) {
					sendToOne(ID, "Please wait until other players have rolled");
					log.info(ID + ", Please wait until other players have rolled");
				}
			}
			else {
				sendToOne(ID, "\nInvalid roll! Please enter an integer between 1 and 6");
				log.info(ID + "\nInvalid roll! Please enter an integer between 1 and 6");
			}
			
			if(rollDone==players.size()) {
				for(int i = 0; i < players.size(); i++) {
					broadcast("\n" + players.get(i).getName() + " roll " + players.get(i).getRoll());
					log.info("\n" + players.get(i).getName() + " roll " + players.get(i).getRoll());
					Boolean foundTarget = false;
					Boolean hit = false;
					String target = players.get(i).getTarget();
					String targetDefense = "UNKNOWN";
					for(int j = 0; j < players.size(); j++) {
						if(players.get(i).getTarget().equalsIgnoreCase(players.get(j).getName())) {
							targetDefense = players.get(j).getDefense();
							hit = engine.processGameLogic(players.get(i).getAttack(), players.get(i).getAttackSpd(), 
									players.get(j).getDefense(), players.get(j).getDefenseSpd());
							if (hit) {
								players.get(j).setWound(players.get(j).getWound() + 1);
							}
						}
					}
					if(hit) {
						broadcast(target + " was successfully hit by " + players.get(i).getName() +
								" using " + players.get(i).getAttack() + " against " + targetDefense + "\n" + 
								target + " suffered 1 wound. \n");
						
						log.info(target + " was successfully hit by " + players.get(i).getName() +
								" using " + players.get(i).getAttack() + " against " + targetDefense + "\n" + 
								target + " suffered 1 wound. \n");
					}
					else {
						broadcast(target + " was NOT hit by " + players.get(i).getName() +
								" using " + players.get(i).getAttack() + " against " + targetDefense + "\n" + 
								target + " did not suffer any wounds \n");
						
						log.info(target + " was NOT hit by " + players.get(i).getName() +
								" using " + players.get(i).getAttack() + " against " + targetDefense + "\n" + 
								target + " did not suffer any wounds \n");
					}
				}
				roundsDone++;
				if (roundsDone < rounds) {
					int nextRound = roundsDone + 1;
					broadcast("End of round " + roundsDone + ". Please enter your select statements for round " +
							nextRound);
					
					log.info("End of round " + roundsDone + ". Please enter your select statements for round " +
							nextRound);
					selectDone = 0;
					rollDone = 0;
				}
				else {
					String results = "";
					for(int i = 0; i < players.size(); i++) {
						results = results.concat("\n"+players.get(i).getName() + ": " + players.get(i).getWound() + " wounds");
						
					}
					broadcast("\nGame Over! Final results are: " + results);
					log.info("\nGame Over! Final results are: " + results);
					broadcast("Thank you for playing :D");
					log.info("Thank you for playing :D");
					broadcast("quit!");
					log.info("quit!");
				}
			}
		}
		
	}

	/** Try and shutdown the client cleanly */
	public synchronized void remove(int ID) {
		if (clients.containsKey(ID)) {
			ServerThread toTerminate = clients.get(ID);
			clients.remove(ID);
			clientCount--;

			toTerminate.close();
			toTerminate = null;
		}
	}
	
	public void broadcast (String msg) {
		 for(Integer key: clients.keySet()) {
			clients.get(key).send(msg + "\n");
		}
	}
	
	public void sendToOne (int ID, String msg) {
		clients.get(ID).send(msg + "\n");
	}
	

	/** Shutdown the server cleanly */
	public void shutdown() {
		Set<Integer> keys = clients.keySet();

		if (thread != null) {
			thread = null;
		}

		try {
			for (Integer key : keys) {
				clients.get(key).close();
				clients.put(key, null);
			}
			clients.clear();
			server.close();
		} catch (IOException e) {
			Trace.getInstance().exception(this,e);
		}
		log.info(String.format("Server Shutdown cleanly %s", server));
		Trace.getInstance().write(this, "Server Shutdown cleanly", server );
		Trace.getInstance().close();
	}
	
}
