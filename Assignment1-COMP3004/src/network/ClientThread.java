package network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Scanner;

import utils.Trace;

public class ClientThread extends Thread {
	
	private Scanner sc = new Scanner(System.in);
	private String target;
	private String attack;
	private int attackSpd;
	private String defense;
	private int defenseSpd;
	private String param;
	private int roll                = 0;
	private Socket         socket   = null;
	private Client         client   = null;
	private BufferedReader streamIn = null;
	private boolean done = false;
	
	public ClientThread(Client client, Socket socket) {  
		this.client = client;
		this.socket = socket;
		this.open();  
		this.start();
	}
	
	public void open () {
		try {  
			streamIn  = new BufferedReader(new InputStreamReader(socket.getInputStream()));
	    } catch(IOException ioe) {  
	    	System.out.println("Error getting input stream");
	    	Trace.getInstance().exception(this,ioe);
			client.stop();
	    }
	}
	
	public String enterParam() {
		System.out.println("Enter target \n");
		target = sc.nextLine();
		System.out.println("Enter attack \n");
		attack = sc.nextLine();
		System.out.println("Enter attack speed \n");
		attackSpd = sc.nextInt();
		System.out.println("Enter defense \n");
		defense = sc.nextLine();
		System.out.println("Enter defense speed \n");
		defenseSpd = sc.nextInt();
		System.out.println("Enter die roll between 1 and 6 \n");
		while(roll<1 || roll>6) {
			roll = sc.nextInt();
			if(roll<1 || roll>6) {
				System.out.println("please enter a valid integer between 1 and 6");
			}
		}
		param = target + " " + attack + " " + attackSpd + " " + defense + " " + defenseSpd + " " + roll; 
		return param;
	}
	
	public void close () {
		done = true;
		try {  
			if (streamIn != null) streamIn.close();
			if (socket != null) socket.close();
			this.socket = null;
			this.streamIn = null;
		} catch(IOException ioe) { 
			Trace.getInstance().exception(this,ioe);
	   }	
	}
	
	public void run() {
		System.out.println("Client Thread " + socket.getLocalPort() + " running.");
		while (!done) { 
			try {
				client.handle(streamIn.readLine());
			} catch(IOException ioe) {  
				Trace.getInstance().exception(this,ioe);
	    }}
	}	

}
