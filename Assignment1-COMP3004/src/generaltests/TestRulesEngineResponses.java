package generaltests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import utils.Config;

import rulesengine.GameRulesEngine;

public class TestRulesEngineResponses {
	GameRulesEngine rEngine;

	/** This will be processed before the Test Class is instantiated */
    @BeforeClass
    public static void BeforeClass() {
        System.out.println("@BeforeClass: TestRulesEngineResponses");
    }
    
    /** This is processed/initialized before each test is conducted */
    @Before
    public void setUp() {
		System.out.println("@Before(): TestRulesEngineResponses");
		rEngine = new GameRulesEngine();
	}
	
    /** This is processed/initialized after each test is conducted */
    @After
    public void tearDown () {
		System.out.println("@After(): TestRulesEngineResponses");
		rEngine = null;
	}
	
    /** This will be processed after the Test Class has been destroyed */
    @AfterClass
    public static void afterClass () {
    	 System.out.println("@AfterClass: TestRulesEngineResponses");
    }
    
    /**This will test the different attacks against different directions where the attack speeds are always 
     * lower than the defense speeds. Since the attack speeds are always lower, the attacks will always hit*/
    @Test
	public void Rule01() {
		System.out.println("@Test(): Rule01");
		
		assertTrue(rEngine.processGameLogic(rEngine.processRoll("Swing", 1), 2, "Dodge", 3));
		 
		assertTrue(rEngine.processGameLogic(rEngine.processRoll("Thrust", 5), 1, "Charge", 2));
		
		assertTrue(rEngine.processGameLogic(rEngine.processRoll("Smash", 4), 3, "Duck", 4));
		
		assertTrue(rEngine.processGameLogic(rEngine.processRoll("Swing", 6), 1, "Charge", 4));
	}
    
    
    /**In this test, the attack speed will always be lower than the defense speed. Therefore, attack's 
     * success will depend on the attacker's die roll and the attack direction chosen. Not all the 
     * attacks will successfully hit in this test*/
    @Test
	public void Rule02() {
		System.out.println("@Test(): Rule02");

		assertTrue(rEngine.processGameLogic(rEngine.processRoll("Swing", 1), 4, "Dodge", 3));
		 
		assertFalse(rEngine.processGameLogic(rEngine.processRoll("Thrust", 5), 3, "Charge", 2));
		
		assertFalse(rEngine.processGameLogic(rEngine.processRoll("Smash", 4), 3, "Duck", 1));
		
		assertFalse(rEngine.processGameLogic(rEngine.processRoll("Swing", 6), 4, "Charge", 2));
	}
    

}
