package generaltests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import utils.Config;

import rulesengine.GameRulesEngine;

public class TestRulesEngineAttacks {
	GameRulesEngine rEngine;

    @BeforeClass
    public static void BeforeClass() {
        System.out.println("@BeforeClass: TestRulesEngineClues");
    }
    
    @Before
    public void setUp() {
		System.out.println("@Before: TestRulesEngineClues");
		rEngine = new GameRulesEngine();
	}
	
    @After
    public void tearDown () {
		System.out.println("@After(): TestRulesEngineClues");
		rEngine = null;
	}
	
    @AfterClass
    public static void afterClass () {
    	 System.out.println("@AfterClass: TestRulesEngineClues");
    }
    
    @Test
	public void Attack01 () {
		System.out.println("@Test(): Attack01");
		rEngine.setState(Config.WAITING);
		String attack = rEngine.processRoll("Thrust", 1);
		assertEquals("Thrust",attack);
	}
    
    @Test
	public void Attack02 () {
		System.out.println("@Test(): Attack02");
		rEngine.setState(Config.WAITING);
		String attack = rEngine.processRoll("Thrust", 2);
		assertEquals("Thrust",attack);
	}
    
    @Test
	public void Attack03 () {
		System.out.println("@Test(): Attack03");
		rEngine.setState(Config.WAITING);
		String attack = rEngine.processRoll("Thrust", 3);
		assertEquals("Smash",attack);
	}
    
    @Test
	public void Attack04 () {
		System.out.println("@Test(): Attack04");
		rEngine.setState(Config.WAITING);
		String attack = rEngine.processRoll("Thrust", 4);
		assertEquals("Smash",attack);
	}
    
    @Test
	public void Attack05 () {
		System.out.println("@Test(): Attack05");
		rEngine.setState(Config.WAITING);
		String attack = rEngine.processRoll("Thrust", 5);
		assertEquals("Swing",attack);
	}
    
    @Test
	public void Attack06 () {
		System.out.println("@Test(): Attack06");
		rEngine.setState(Config.WAITING);
		String attack = rEngine.processRoll("Thrust", 6);
		assertEquals("Swing",attack);
	}
	
    @Test
	public void Attack07 () {
		System.out.println("@Test(): Attack07");
		rEngine.setState(Config.WAITING);
		String attack = rEngine.processRoll("Swing", 1);
		assertEquals("Swing",attack);
    }
    
    @Test
	public void Attack08 () {
		System.out.println("@Test(): Attack08");
		rEngine.setState(Config.WAITING);
		String attack = rEngine.processRoll("Swing", 2);
		assertEquals("Swing",attack);
    }
    
    @Test
	public void Attack09 () {
		System.out.println("@Test(): Attack09");
		rEngine.setState(Config.WAITING);
		String attack = rEngine.processRoll("Swing", 3);
		assertEquals("Thrust",attack);
    }
    
    @Test
	public void Attack10 () {
		System.out.println("@Test(): Attack10");
		rEngine.setState(Config.WAITING);
		String attack = rEngine.processRoll("Swing", 4);
		assertEquals("Thrust",attack);
    }
    
    @Test
	public void Attack11 () {
		System.out.println("@Test(): Attack11");
		rEngine.setState(Config.WAITING);
		String attack = rEngine.processRoll("Swing", 5);
		assertEquals("Smash",attack);
    }
    
    @Test
	public void Attack12 () {
		System.out.println("@Test(): Attack12");
		rEngine.setState(Config.WAITING);
		String attack = rEngine.processRoll("Swing", 6);
		assertEquals("Smash",attack);
    }
    
    @Test
	public void Attack13 () {
		System.out.println("@Test(): Attack13");
		rEngine.setState(Config.WAITING);
		String attack = rEngine.processRoll("Smash", 1);
		assertEquals("Smash",attack);
    }
    
    @Test
	public void Attack14 () {
		System.out.println("@Test(): Attack14");
		rEngine.setState(Config.WAITING);
		String attack = rEngine.processRoll("Smash", 2);
		assertEquals("Smash",attack);
    }
    
    @Test
	public void Attack15 () {
		System.out.println("@Test(): Attack15");
		rEngine.setState(Config.WAITING);
		String attack = rEngine.processRoll("Smash", 3);
		assertEquals("Swing",attack);
    }
    
    @Test
	public void Attack16 () {
		System.out.println("@Test(): Attack16");
		rEngine.setState(Config.WAITING);
		String attack = rEngine.processRoll("Smash", 4);
		assertEquals("Swing",attack);
    }
    
    @Test
	public void Attack17 () {
		System.out.println("@Test(): Attack17");
		rEngine.setState(Config.WAITING);
		String attack = rEngine.processRoll("Smash", 5);
		assertEquals("Thrust",attack);
    }
    
    @Test
	public void Attack18 () {
		System.out.println("@Test(): Attack18");
		rEngine.setState(Config.WAITING);
		String attack = rEngine.processRoll("Smash", 6);
		assertEquals("Thrust",attack);
    }
}
