package generaltests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import utils.Config;

import rulesengine.GameRulesEngine;

public class TestRulesEngineDefenses {
	GameRulesEngine rEngine;

    @BeforeClass
    public static void BeforeClass() {
        System.out.println("@BeforeClass: TestRulesEngineClues");
    }
    
    @Before
    public void setUp() {
		System.out.println("@Before: TestRulesEngineClues");
		rEngine = new GameRulesEngine();
	}
	
    @After
    public void tearDown () {
		System.out.println("@After(): TestRulesEngineClues");
		rEngine = null;
	}
	
    @AfterClass
    public static void afterClass () {
    	 System.out.println("@AfterClass: TestRulesEngineClues");
    }
    
    @Test
	public void Defense01 () {
		System.out.println("@Test(): Defense01");
		rEngine.setState(Config.WAITING);
		String defenses = rEngine.defenses[0];
		assertEquals("Charge",defenses);
	}
	
    @Test
	public void Defense02 () {
		System.out.println("@Test(): Defense02");
		rEngine.setState(Config.WAITING);
		String defenses = rEngine.defenses[1];
		assertEquals("Dodge",defenses);
	}
    
    @Test
	public void Defense03 () {
		System.out.println("@Test(): Defense03");
		rEngine.setState(Config.WAITING);
		String defenses = rEngine.defenses[2];
		assertEquals("Duck",defenses);
	}
}
