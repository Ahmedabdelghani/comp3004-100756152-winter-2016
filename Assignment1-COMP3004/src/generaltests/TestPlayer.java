package generaltests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import resources.Player;
import utils.Config;

public class TestPlayer {
	Player player;

	@Before
    public void setUp() {
		System.out.println("@Before: TestingPlayer");
		player = new Player("Fred");
	}
	
    @After
    public void tearDown () {
		System.out.println("@After(): TestingPlayer");
		player = null;
	}
    
    @Test
	public void NameTest() {
    	player.setName("joe");
		assertEquals("joe",player.getName());
	}
    
    @Test
	public void AttackTest() {
    	player.setAttack("swing");
		assertEquals("swing",player.getAttack());
	}
    
    @Test
	public void DefenseTest() {
    	player.setDefense("duck");
		assertEquals("duck",player.getDefense());
	}
    
    @Test
	public void TargetTest() {
    	player.setTarget("jill");
		assertEquals("jill",player.getTarget());
	}

    @Test
	public void AttackSpeedTest() {
    	player.setAttackSpd(2);
		assertEquals(2,player.getAttackSpd());
	}
    
    @Test
    public void DefenseSpeedTest() {
    	player.setDefenseSpd(3);
		assertEquals(3,player.getDefenseSpd());
	}
    
    @Test
    public void RollTest() {
    	player.setRoll(3);
		assertEquals(3,player.getRoll());
	}
    
    @Test
    public void WoundTest() {
    	player.setWound(1);
		assertEquals(1,player.getWound());
	}
}
