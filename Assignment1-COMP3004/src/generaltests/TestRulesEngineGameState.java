package generaltests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import rulesengine.GameRulesEngine;
import utils.Config;

import org.junit.Before;

public class TestRulesEngineGameState {
	GameRulesEngine rEngine;

	/** This will be processed before the Test Class is instantiated */
    @BeforeClass
    public static void BeforeClass() {
        System.out.println("@BeforeClass: TestRulesEngine");
    }

    /** This is processed/initialized before each test is conducted */
    @Before
    public void setUp() {
		System.out.println("@Before(): TestRulesEngine");
		rEngine = new GameRulesEngine();
	}
	
    /** This is processed/initialized after each test is conducted */
    @After
    public void tearDown () {
		System.out.println("@After(): TestRulesEngine");
		rEngine = null;
	}
	
    /** This will be processed after the Test Class has been destroyed */
    @AfterClass
    public static void afterClass () {
    	 System.out.println("@AfterClass: TestRulesEngine");
    }
    
    /** Each unit test is annotated with the @Test Annotation */
    @Test
	public void WaitingState () {
    	System.out.println("@Test: WaitingState");
		rEngine.setState(Config.OVER_CAPACITY);
		assertEquals(Config.WAITING, rEngine.processGameState(1));
	}
	
    @Test
	public void ReadyState () {
    	System.out.println("@Test: ReadyState");
		rEngine.setState(Config.WAITING);
		assertEquals(Config.READY, rEngine.processGameState(2));
	}
    
    @Test
	public void OverCapacityState () {
    	System.out.println("@Test: ReadyState");
		rEngine.setState(Config.WAITING);
		assertEquals(Config.OVER_CAPACITY, rEngine.processGameState(5));
	}

}
