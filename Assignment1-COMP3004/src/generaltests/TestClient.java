package generaltests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import network.Client;
import utils.Config;

public class TestClient {

	@Test
	public void ClientIdTest() {
		Client client = new Client(Config.DEFAULT_HOST, Config.DEFAULT_PORT, "joe");
		assertNotNull(client.getID());
	}
	
	
}
