package generaltests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
   TestRulesEngineGameState.class,
   TestRulesEngineAttacks.class,
   TestRulesEngineDefenses.class,
   TestRulesEngineResponses.class,
   TestPlayer.class,
   TestGameServer.class,
   TestClient.class
})

public class TestSuite {   
} 

