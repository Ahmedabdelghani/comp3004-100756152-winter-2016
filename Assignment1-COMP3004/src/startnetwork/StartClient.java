package startnetwork;

import java.util.Scanner;

import network.Client;
import utils.Config;

public class StartClient {
	private static Scanner sc = new Scanner(System.in);
	private static String name;
	public static void main(String[] argv) {
		
		System.out.println("Please enter your name\n");
		name = sc.nextLine();
		System.out.println("Enter the IP of the Server Machine: ");
		String ip = sc.nextLine();
		System.out.println("Enter the Port Number of the server Machine: ");
		int port = sc.nextInt(); 
		System.out.println("\n");
		new Client(ip, port, name);
	}
}