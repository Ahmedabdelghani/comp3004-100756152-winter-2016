package startnetwork;

import java.io.Console;
import java.io.IOException;
import java.util.Scanner;

import network.GameServer;
import utils.Config;
import utils.Level;
import utils.Trace;

@SuppressWarnings("unused")
public class StartGameServer {
	
	private static Boolean done = Boolean.FALSE;
	private static Boolean started = Boolean.FALSE;

	private static Scanner sc = new Scanner(System.in);
	private static GameServer appServer = null;
	private static int port;
	private static int numPlayers;
	private static int rounds;
	
	public static void main(String[] argv) {
	
		Console c = System.console();
		if (c == null) {
			System.err.println("No System Console....");
			System.err.println("Use IDE Console....");
		}
		
		do {
			String input = sc.nextLine();
			
			if (input.equalsIgnoreCase("START") && !started)
			{
				System.out.println("Starting server ...");
				Trace.getInstance().setLevel(Level.STDOUT);
				
				System.out.println("Please enter the number of rounds you would like to play");
				//numPlayers = Integer.valueOf(sc.nextLine());
				while (rounds == 0) {
					rounds = sc.nextInt();
					
					if(rounds == 0) {
						System.out.println("Please enter an integer higher than 0.");
					}
				}
				
				System.out.println("Please enter the number of players you want. Players must be between 2 and 4.");
				//numPlayers = Integer.valueOf(sc.nextLine());
				while (numPlayers < Config.MINIMUM_PLAYERS) {
					numPlayers = sc.nextInt();
					
					if(numPlayers < Config.MINIMUM_PLAYERS || numPlayers > Config.MAXIMUM_PLAYERS) {
						System.out.println("Number must be between 2 and 4. Please try again.");
					}
				}
				
				System.out.println("the players are: " + numPlayers);
				appServer = new GameServer(Config.DEFAULT_PORT, numPlayers, rounds);
				started = Boolean.TRUE;
			}
			
			if (input.equalsIgnoreCase("SHUTDOWN") && started)
			{
				System.out.println("Shutting server down ...");
				Trace.getInstance().close();
				appServer.shutdown();
				started = Boolean.FALSE;
				done = Boolean.TRUE;
			}			
		} while (!done);

		System.exit(1);
	}
}
