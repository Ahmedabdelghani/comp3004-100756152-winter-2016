This game should be launched by running StartGameServer and StartClient.

once you run StartGameServer, you should enter "start", after which you will be prompted to enter 
the number of rounds and players you would like.

once you run StartClient, you will be prompted to enter a name, the server's IP address, and port.
If you are running the client on the same machine as the server's you should enter "localhost" 
when prompted for the server's IP address. 

When prompted for the port, enter 3000

Enter your select as follows: "select (your name) (target's name) (attack) 
(attack speed) (defense) (defense speed)"

Enter your roll as follows: "roll (your name) (roll number)

The game cannot successfully connect to multiple clients on a network. It can 
only connect to clients running on the localhost.

The Rules Engine for the game correctly picks the appropriate attack based on
the roll which the used has picked. It also correctly processes the attacks, 
defenses, and their respective speeds correctly in order to decide whether 
the attack was successful or not. 

The server can successfully limit the number of players joining the game based 
on the number of players eneterd upon starting the server. It also limits the 
number of rounds to what was entered upon starting it. 
The server echos each player entry to all other players. It also checks to make 
sure the sum of speeds entered by a player add up to at least 4. 
The server checks to make sure the die roll does not exceed 6 or fall below 0.
The server indicates whether each attack was successful or not at the end of each 
round. It also displays the total number of wounds accumulated by all players 
at the end of the game. 

The networking solution used is the same one utilized by Howard in his Chat Example in class

The TestSuit found inside the generaltests package takes care of running all other test 
files in the project. 

The logs recordede can be found in the folder "logs" inside the project's folder